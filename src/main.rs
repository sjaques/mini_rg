use std::env;
use std::process;

fn main() {
    let case_sensitive = env::var("CASE_SENSITIVE").is_err();

    let config = mini_rg::Config::new(env::args(), case_sensitive).unwrap_or_else(|error| {
        eprintln!("Program parsing error: {}", error);
        process::exit(1);
    });

    if let Err(e) = mini_rg::run(&config) {
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}

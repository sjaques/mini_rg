use std::fs;
use std::error::Error;

pub struct Config {
    pub query: String,
    pub filename: String,
    pub case_sensitive: bool,
}

impl Config {
    pub fn new(mut arguments: std::env::Args, case_sensitive: bool) -> Result<Config, &'static str> {
        arguments.next();  // skip executable name

        let query = match arguments.next() {
            Some(query) => query,
            None => return Err("Missing query argument")
        };

        let filename = match arguments.next() {
            Some(filename) => filename,
            None => return Err("Missing filename argument")
        };

        Ok(Config{query, filename, case_sensitive})
    }
}

pub fn run(config: &Config) -> Result<(), Box<dyn Error>> {
    let file_content = fs::read_to_string(&config.filename)?;

    let results = if config.case_sensitive {
        search_case_sensitive(&config.query, &file_content)
    } else {
        search_case_insensitive(&config.query, &file_content)
    };

    for line in results {
        println!("{}", line);
    }

    Ok(())
}

pub fn search_case_sensitive<'a>(query: &str, text: &'a str) -> Vec<&'a str> {
    text.lines().filter(|line| line.contains(query))
                .collect()
}

pub fn search_case_insensitive<'a>(query: &str, text: &'a str) -> Vec<&'a str> {
    text.lines().filter(|line| line.to_lowercase().contains(&query.to_lowercase()))
                .collect()
}
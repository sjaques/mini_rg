#[cfg(test)]

mod tests {

#[test]
fn test_find_one_word_case_sensitive() {
    let word = "brevity";
    let text = "\"The quick brown fox jumps over the lazy dog\"
is an English-language pangram,
a sentence that contains all of the letters of the English alphabet.
Owing to its brevity and coherence,
it has become widely known.";

    let expected_result = "Owing to its brevity and coherence,";
    let results = mini_rg::search_case_sensitive(&word, text);

    assert_eq!(results, vec![expected_result]);
}

#[test]
fn test_find_one_word_case_insensitive() {
    let word = "wIdElY";
    let text = "\"The quick brown fox jumps over the lazy dog\"
is an English-language pangram,
a sentence that contains all of the letters of the English alphabet.
Owing to its brevity and coherence,
it has become widely known.";

    let expected_result = "it has become widely known.";
    let results = mini_rg::search_case_insensitive(&word, text);

    assert_eq!(results, vec![expected_result]);
}

}  // mod tests